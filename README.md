# A4-Isiah-Simas

This assignment was originally done for the Emerging Interfaces class within the IT Innovation and Design program at Conestoga College.

This repository has been moved to BitBucket to complete the requirements for an assignment in the Project Development course.

## Build and Install

Please open the IDE of your choice and initiate a live server to run the web application.

### For VS Code Users

1. Open the folder with VS Code
2. Ensure [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) is installed, if not install it.
3. Start the server, and the application will be ready to run.

## License

Note: MIT License has been added because of it's ease of use.

MIT License

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
